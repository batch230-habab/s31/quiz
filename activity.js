const http = require("http");


let port = 3000;

const server = http.createServer((request, response) => {
    if(request.url == '/login'){
        response.writeHead(200, {'content-type' : 'text/plain'});
        response.end ('Welcome to the log in page')
    }else{
        response.writeHead(404, {'Content-type': 'text/plain'});
        response.end(`I'm sorry the page you are looking for cannot be found`);
    }
})
server.listen(port);

console.log('Server is now running at port '+ port)